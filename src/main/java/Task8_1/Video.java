package Task8_1;

import java.util.Objects;

class Video implements Comparable<Video> {
    String author, title, comments;
    int duration;

    public Video(String author, String title, int duration, String comments) {
        this.author = author;
        this.title = title;
        this.duration = duration;
        this.comments = comments;

    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Video video)) return false;
        return Objects.equals(author, video.author) && Objects.equals(title, video.title) && Objects.equals(duration, video.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, title, duration);
    }

    @Override
    public String toString() {
        return "Video{" +
                "author=" + author +
                ", title=" + title +
                ", duration=" + duration + " seconds" +
                ", comments=" + comments +
                '}';
    }

    @Override
    public int compareTo(Video video) {
        return this.duration - video.duration;
    }
}

