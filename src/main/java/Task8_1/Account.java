package Task8_1;

import java.util.*;

public class Account{
    private static List<Video> likedVideos;

    public Account() {
        likedVideos = new ArrayList<>();
    }
    //вывод на экран всех понравившихся видео (обход списка должен быть сделан через объект итератора)
    public void printLikeVideo(){
        Iterator<Video> iterator = likedVideos.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    //добавление понравившегося видео
    public void addLikeVideo(Video video){
        likedVideos.add(video);
    }

    //удаление видео из списка понравившегося
    public void deleteLikeVideo(Video video){
        likedVideos.remove(video);
    }

    //вывод списка всех понравившихся видео, отсортированных по возрастанию длительности видео (сделать через Comparable)
    public void displaySortedByDurationWithComparable(){
        likedVideos.sort(Video::compareTo);
        for (Video video : likedVideos) {
            System.out.println(video);
        }
    }

    //вывод списка всех понравившихся видео, отсортированных по возрастанию длительности видео (сделать через Comparator)
    public void displaySortedByDurationWithComparator(){
        likedVideos.sort(Comparator.comparingInt(v -> v.duration));
        for (Video video : likedVideos) {
            System.out.println(video);
        }

    }

    //очистка списка понравившихся видео от дубликатов
    public void clearDuplicates(){
        likedVideos = new ArrayList<>(new HashSet<>(likedVideos));
    }
}
