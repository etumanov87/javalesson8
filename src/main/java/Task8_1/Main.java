package Task8_1;

import com.github.javafaker.Faker;

public class Main {
    public static void main(String[] args) {
        Faker faker = new Faker();
        Account account = new Account();

        for (int i = 0; i < 30; i++) {
            account.addLikeVideo(new Video(
                    faker.name().fullName(),
                    faker.name().title(),
                    faker.number().numberBetween(0, 100),
                    faker.address().fullAddress())
            );
        }
        account.printLikeVideo();
        System.out.println("-------------------------");
        account.displaySortedByDurationWithComparable();
        System.out.println("-------------------------");
        account.displaySortedByDurationWithComparator();
        System.out.println("-------------------------");
        account.addLikeVideo(new Video("Test Delete", "test delete Like Video", 102, "test Comment from delete"));
        account.addLikeVideo(new Video("Test Duplicates", "test clear Duplicates", 120, "test Comment from Duplicates"));
        account.addLikeVideo(new Video("Test Duplicates", "test clear Duplicates", 120, "test Comment2 from Duplicates"));
        account.printLikeVideo();
        account.deleteLikeVideo(new Video("Test Delete", "test delete Like Video", 102, "test Comment from delete"));
        System.out.println("-------------------------");
        account.printLikeVideo();
        System.out.println("-------------------------");
        account.clearDuplicates();
        account.displaySortedByDurationWithComparable();
    }
}
