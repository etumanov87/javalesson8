package Task8_2;

import java.util.HashMap;
import java.util.Map;

public class Translator {
    private Map<String, String> dictionary = new HashMap<>();

    //добавляет в базу новые слова с переводом
    //на одно английское слово можно хранить несколько вариантов русского перевода
    public void addTranslatorFromDictionary(String wordEn, String translatedWordToRu) {
        if (dictionary.containsKey(wordEn)) {
            dictionary.put(wordEn, dictionary.get(wordEn) + "," + translatedWordToRu);
        } else {
            dictionary.put(wordEn, translatedWordToRu);
        }
    }

    //возвращает русский перевод для переданного английского слова
    public void translateWordEnToRu(String wordEn) {
        if (dictionary.get(wordEn) == null) {
            System.out.println("В словаре нету перевода на это слово.");
        } else {
            System.out.println(dictionary.get(wordEn));
        }
    }

    //удаляет из базы слово с переводом
    public void deleteWordWithTranslation(String wordEn) {
        dictionary.remove(wordEn);
    }


}
