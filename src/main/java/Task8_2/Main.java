package Task8_2;

public class Main {
    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.addTranslatorFromDictionary("Red", "Красный");
        translator.addTranslatorFromDictionary("Yellow", "Желтый");
        translator.addTranslatorFromDictionary("Green", "Зеленый");
        translator.addTranslatorFromDictionary("Blue", "Голубой");
        translator.addTranslatorFromDictionary("Brown", "Коричневый");
        translator.addTranslatorFromDictionary("White", "Белый");
        translator.addTranslatorFromDictionary("Orange", "Оранжевый");
        translator.addTranslatorFromDictionary("Pink", "Розовый");
        translator.addTranslatorFromDictionary("Gray", "Серый");
        translator.addTranslatorFromDictionary("Black", "Черный");

        translator.translateWordEnToRu("Brown");
        translator.translateWordEnToRu("Black");
        translator.translateWordEnToRu("Red");

        translator.deleteWordWithTranslation("Pink");
        translator.translateWordEnToRu("Pink");

        translator.translateWordEnToRu("Red");
        translator.addTranslatorFromDictionary("Red", "Багровый");
        translator.translateWordEnToRu("Red");
    }
}
